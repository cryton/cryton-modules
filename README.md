![Coverage](https://gitlab.ics.muni.cz/cryton/cryton-modules/badges/master/coverage.svg)

[//]: # (TODO: add badges for python versions, black, pylint, flake8, unit tests, integration tests)

# PROJECT HAS BEEN MOVED
The project has been moved to https://gitlab.ics.muni.cz/cryton/cryton. For more information check the [documentation](https://cryton.gitlab-pages.ics.muni.cz/).

# Cryton Modules
Cryton (attack) modules is a collection of Python scripts with the goal of orchestrating known offensive security tools 
(Nmap, Metasploit, THC Hydra, etc.). Although this is their intended purpose, they are still Python scripts, and therefore 
any custom-made script can be used similarly.

Cryton toolset is tested and targeted primarily on **Debian** and **Kali Linux**. Please keep in mind that **only 
the latest version is supported** and issues regarding different OS or distributions may **not** be resolved.

For more information see the [documentation](https://cryton.gitlab-pages.ics.muni.cz/cryton-documentation/latest/components/modules/).

## Quick-start
Attack modules are orchestrated by [Cryton Worker](https://gitlab.ics.muni.cz/cryton/cryton-worker). For now, their installation
will be covered in the [Worker's quick-start](https://gitlab.ics.muni.cz/cryton/cryton-worker#quick-start) section.

For more information see the [documentation](https://cryton.gitlab-pages.ics.muni.cz/cryton-documentation/latest/components/modules/).

## Contributing
Contributions are welcome. Please **contribute to the [project mirror](https://gitlab.com/cryton-toolset/cryton-modules)** on gitlab.com.
For more information see the [contribution page](https://cryton.gitlab-pages.ics.muni.cz/cryton-documentation/latest/contribution-guide/).
